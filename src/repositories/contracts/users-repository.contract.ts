import { User } from '../../models/users/entities/user.entity'

export interface IUsersRepository {
  create(user: User): Promise<void>
  findAll(): Promise<User[]>
  findByEmail(email: string): Promise<User>
}
