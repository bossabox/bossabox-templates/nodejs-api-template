import { IUsersRepository } from '../../contracts/users-repository.contract'
import { UserMapper } from '../../../models/users/mappers/user.mapper'
import { User } from '../../../models/users/entities/user.entity'

export class InMemoryUserRepository implements IUsersRepository {
  private users = []

  async findAll(): Promise<User[]> {
    return this.users.map(UserMapper.toDomain)
  }

  async findByEmail(email: string): Promise<User> {
    const user = this.users.find(user => user.email === email)

    if (!user) return null

    return UserMapper.toDomain(user)
  }

  async create(user: User): Promise<void> {
    const data = UserMapper.toDto(user)

    this.users.push(data)
  }
}
