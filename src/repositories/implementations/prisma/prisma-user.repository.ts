import { IUsersRepository } from '../../contracts/users-repository.contract'
import { UserMapper } from '../../../models/users/mappers/user.mapper'
import { User } from '../../../models/users/entities/user.entity'
import { prisma } from '../../../config/prisma/client'

export class PrismaUserRepository implements IUsersRepository {
  async findAll(): Promise<User[]> {
    const users = await prisma.user.findMany()

    return users.map(UserMapper.toDomain)
  }

  async findByEmail(email: string): Promise<User> {
    const user = await prisma.user.findUnique({ where: { email } })

    if (!user) return null

    return UserMapper.toDomain(user)
  }

  async create(user: User): Promise<void> {
    const data = UserMapper.toDto(user)

    await prisma.user.create({ data })
  }
}
