import { IUsersRepository } from '../../repositories/contracts/users-repository.contract'

export class ListUsersService {
  constructor(private usersRepository: IUsersRepository) {}

  public async exec() {
    return this.usersRepository.findAll()
  }
}
