import { CreateUserService } from './create-user.service'

import { UserAlreadyExistsError } from './errors/user-already-exists.error'

import { User } from '../../models/users/entities/user.entity'

import { InMemoryUserRepository } from '../../repositories/implementations/inMemory/in-memory-user.repository'

describe('Given CreateUserService', () => {
  const inMemoryUserRepository = new InMemoryUserRepository()
  const sut = new CreateUserService(inMemoryUserRepository)

  describe('when user exists', () => {
    beforeEach(async () => {
      await inMemoryUserRepository.create(
        new User({ email: 'jhondoe@contact.com', name: 'Jhon Doe' })
      )
    })

    it('should be able to throw an error', async () => {
      await expect(
        sut.exec({
          email: 'jhondoe@contact.com',
          name: 'Jhon Doe',
        })
      ).rejects.toEqual(new UserAlreadyExistsError('jhondoe@contact.com'))
    })
  })
})
