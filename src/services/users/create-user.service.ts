import { UserAlreadyExistsError } from './errors/user-already-exists.error'

import { IUsersRepository } from '../../repositories/contracts/users-repository.contract'
import { User } from '../../models/users/entities/user.entity'

type Props = {
  name: string
  email: string
}

export class CreateUserService {
  constructor(private usersRepository: IUsersRepository) {}

  public async exec(props: Props) {
    const user = new User(props)

    const emailAlreadyExists = await this.usersRepository.findByEmail(
      user.props.email
    )

    if (emailAlreadyExists) throw new UserAlreadyExistsError(user.props.email)

    await this.usersRepository.create(user)

    return user
  }
}
