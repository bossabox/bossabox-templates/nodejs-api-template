import { Request, Response } from 'express'

import { InvalidEmailError } from '../models/users/errors/user-invalid-email.error'
import { InvalidNameError } from '../models/users/errors/user-invalid-name.error'
import { PrismaUserRepository } from '../repositories/implementations/prisma/prisma-user.repository'
import { ListUsersService } from '../services/users/list-users.service'
import { CreateUserService } from '../services/users/create-user.service'
import { UserAlreadyExistsError } from '../services/users/errors/user-already-exists.error'

export class UserController {
  public static async index(request: Request, response: Response) {
    try {
      const listUsersService = new ListUsersService(new PrismaUserRepository())
      const users = await listUsersService.exec()

      response.json(users)
    } catch (error) {
      switch (error.constructor) {
        default:
          response.status(500).json({ message: 'Internal server error.' })
      }
    }
  }

  public static async create(request: Request, response: Response) {
    try {
      const { name, email } = request.body

      const createUser = new CreateUserService(new PrismaUserRepository())
      const user = await createUser.exec({ name, email })

      response.status(201).json(user)
    } catch (error) {
      switch (error.constructor) {
        case InvalidNameError:
        case InvalidEmailError:
        case UserAlreadyExistsError:
          response.status(400).json({ message: error.message })
          break
        default:
          response.status(500).json({ message: 'Internal server error.' })
      }
    }
  }
}
