import { config } from 'dotenv'

import util from 'util'
import crypto from 'crypto'
import { exec } from 'child_process'

const prismaBinary = './node_modules/.bin/prisma'

export default async () => {
  console.info('\nMontando suíte de testes...')

  // Carrega a env do .env.test para poder concatenar com o schema randomUUID
  config({ path: '.env.test' })

  const execSync = util.promisify(exec)

  // Gera um identificador de esquema exclusivo para este contexto de teste
  global.__SCHEMA__ = `test_${crypto.randomUUID()}`

  // Gera uma string de conexão para o esquema de teste
  process.env.DATABASE_URL = `${process.env.DATABASE_URL}?schema=${global.__SCHEMA__}`

  // Execute as migrações para garantir que nosso esquema tenha a estrutura necessária para os testes
  await execSync(`${prismaBinary} migrate deploy`)

  console.info('Suíte pronta. Iniciando testes...\n')
}
